<?php

/**
 * @author      Diego Palumbo
 * @version     0.1
 */

require(__DIR__ . "/../entity/UsersApi.php");
require(__DIR__ . "/../client/Web.php");


$sample = new UsersApi('+393803191348');
$sample->post();

echo "WRAPPER: ";
var_dump($sample);

$client = new Web();
$response = $client->call($sample);
echo "RESPONSE: ";
var_dump($response);
