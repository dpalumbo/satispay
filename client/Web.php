<?php
/**
 * File:        Web.php
 * @author      DIEGO PALUMBO
 * @version     0.1
 */

require_once(__DIR__ . '/../conf/config.php');

/**
 * Webservice Client
 */
class Web
{


    public function __construct()
    {
    }

    /**
     * Call satis-pay Services
     *
     * @param Api $request
     *
     * @return string satispay JSON response
     */
    public function call(Api $request)
    {
        if (!$ch = curl_init())
        {
            throw new \Exception('could not initialize curl');
        }
        curl_setopt($ch, CURLOPT_URL, $this->_getUrl($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request->getMethod());
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request->getHeader());
        if ($request->getMethod() == 'POST')
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->getContent()));
        $response = curl_exec($ch);
        $err = curl_error($ch);

        if ($err) {
            throw new \Exception("cURL Error #:" . $err);
        }

        curl_close($ch);

        return $response;
    }

    /**
     * Get url associated to a specific service
     *
     * @return string URL for the service
     */
    private function _getUrl(Api $req)
    {
        return BASE_URL . $req->getEndpoint();
    }
}
